<!DOCTYPE html>
<html>
<head>
	<title>Data Karyawan</title>
	<base href="<?php echo base_url() ?>">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- bootstrap 3.0.2 -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/AdminLTE.css" rel="stylesheet" type="text/css" />
</head>
<body onload="print()">
	<center>
		<table>
			<tr>
				<td>
					<img src="gambar/81.gif" style="width: 80px; height: 80px;">
				</td>
				<td>
					<center>
						<h3>PT. Delapan Satu Mandiri</h3>
					<h5>Jl. Soekarno - Hatta No. 105 Bandung 40222 Telp. (022)6033894 Fax. (022)6033619 </h5>
					</center>
				</td>
			</tr>
		</table>
		<h4>Laporan Gaji Karyawan</h4>
	</center>
	<hr>
	<table class="table table-bordered">
		<thead>
			<tr  class="bg-black">
				<th>No.</th>
				<th>Nik</th>
				<th>Nama</th>
				<th>Jabatan</th>
				<th>Gaji Pokok</th>
				<th>Tunjangan Kesehatan</th>
				<th>Tunjangan Tranportasi</th>
				<th>Tunjangan Pendidikan</th>
				<th>Tunjangan Keluarga</th>
			</tr>
		</thead>
		<tbody>
			<?php 
			$no = 1;
			foreach ($rw->result() as $row) {
			 ?>
			<tr>
				<td><?php echo $no++; ?></td>
				<td><?php echo $row->nik; ?></td>
				<td><?php echo $row->nama; ?></td>
				<td><?php echo $row->pekerjaan; ?></td>
				<td><?php echo $row->gapok; ?></td>
				<td><?php echo $row->tukes; ?></td>
				<td><?php echo $row->tutra; ?></td>
				<td><?php echo $row->tupen; ?></td>
				<td><?php echo $row->tukel; ?></td>
			</tr>
			<?php } ?>
		</tbody>
	</table>

</body>
</html>